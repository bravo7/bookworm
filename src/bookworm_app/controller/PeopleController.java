package bookworm_app.controller;
import java.util.ArrayList;

import bookworm_app.dbaccess.PeopleAccess;
import bookworm_app.model.People;;
public class PeopleController {
	public static ArrayList<String> getPeopleString()
	{
		ArrayList<String> result = new ArrayList<String>();
		ArrayList<People> peoples = PeopleAccess.getPeoples();
		if(peoples.size() > 0)
		{
			for(People people : peoples)
			{
				result.add(people.getFirstname() + " "+people.getLastname());
			}
		}
		return result;
	}
	public static ArrayList<People> getPeoples()
	{
		return PeopleAccess.getPeoples();
	}
	public static People getPeopleById(int id)
	{
		return PeopleAccess.getPeopleById(id);
	}
	public static String[] getPeopleToArray()
	{	
		ArrayList<String> res = new ArrayList();
		for(People p : getPeoples())
		{
			res.add(p.getFullName());
		}
		return (String[]) res.toArray(new String[res.size()]);
	}
	public static boolean createPeople(People people)
	{
		String firstname = people.getFirstname();
		String lastname = people.getLastname();		
		int id = people.getId();
		firstname = firstname.replace("'","''");
		lastname = lastname.replace("'", "''");	
		People p = new People(id, firstname, lastname, people.getGrade());
		Boolean hasBeenCreated = PeopleAccess.createPeople(p);
		return hasBeenCreated;
	}
	public static boolean editPeople(People p)
	{
		boolean res;
		String firstName = p.getFirstname();
		String lastName = p.getLastname();
		String grade = p.getGrade();
		res = PeopleAccess.editPeople(p.getId(),firstName, lastName,grade);
		return res;		
	}
	public static boolean deletePeople(int id)
	{
		return PeopleAccess.deletePeople(id);
	}
}
