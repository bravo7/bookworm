package bookworm_app.view.components.screenmenu;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bookworm_app.Main;
import bookworm_app.model.People;

public class MemberPopUp extends JFrame{

	private MemberPanel owner;
	private boolean modeCreation = true;
	public MemberPopUp(MemberPanel owner, People people, int index)
	{
		super();
		this.owner = owner;
		if(people != null)
			modeCreation = false;
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		this.setSize(500, 200);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		this.setContentPane(this.buildContent(people, index));
		this.setResizable(false);
		this.setVisible(true);
	}
	
	public JPanel buildContent(People people, int index)
	{
		MemberPopUp that = this;
		JPanel content = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.insets = new Insets(8, 10, 5, 8);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		content.add(new JLabel("Firstname : "),c);
		c.gridy++;
		content.add(new JLabel("Lastname : "),c);
		c.gridy++;
		content.add(new JLabel("Grade : "),c);
		c.gridy = 0;
		c.gridx = 1;
		JTextField jFirstname = new JTextField(13);
		if(!modeCreation)
			jFirstname.setText(people.getFirstname());
		content.add(jFirstname,c);
		c.gridy++;
		JTextField jName = new JTextField(13);
		if(!modeCreation)
			jName.setText(people.getLastname());
		content.add(jName,c);
		c.gridy++;
		JTextField jGrade = new JTextField(13);
		if(!modeCreation)
			jGrade.setText(people.getGrade());
		content.add(jGrade,c);
		c.gridy++;
		c.gridx = 1;
		JButton b = new JButton("Ok");
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				People p;
				p = new People(-1,jFirstname.getText(), jName.getText(),jGrade.getText());
				if(that.modeCreation)
					that.owner.setNewPeople(p);
				else
				{
					p.setId(people.getId());
					that.owner.setPeople(p, index);
				}
				that.dispose();
				
			}
		});
		content.add(b,c);
		
		return content;
	}
}