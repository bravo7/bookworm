package bookworm_app.dbaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;

import bookworm_app.Main;
import bookworm_app.model.Borrow;
import bookworm_app.model.People;
import bookworm_app.model.dbaccess.utils.DBConnectionBookWorm;

public class BorrowAccess {
	public static boolean createBorrow(Borrow borrow)
	{
		String req = "INSERT INTO borrow (peopleid ,booktitle, borrowdate) VALUES "
				+ "("+borrow.getPeopleId()+",'"+borrow.getBookTitle()+"','"+ borrow.getStringDate()+"'); ";
		try {
			int result = (int) DBConnectionBookWorm.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	public static ArrayList<Borrow> getBorrows()
	{
		ArrayList<Borrow> resultList = new ArrayList<Borrow>();
		String req = "SELECT * FROM borrow";
		try {
			ResultSet result = (ResultSet) DBConnectionBookWorm.doRequest(req);
			while (result.next()) {
				Borrow p = new Borrow(result.getInt("peopleid"),result.getString("booktitle"), result.getString("borrowdate"));		
				resultList.add(p);
			}		
			
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return resultList;
	}
	public static boolean deleteBorrow(String bookTitle)
	{
		String req = "DELETE FROM borrow WHERE booktitle = '"+bookTitle+"';";
		try {
			int result = (int)DBConnectionBookWorm.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	public static boolean isFree(String title)
	{
		boolean exist = false;
		String req = "SELECT * FROM borrow WHERE booktitle='"+title+"';";
		try {
			ResultSet result = (ResultSet) DBConnectionBookWorm.doRequest(req);
			if(!result.next())
				exist = true;
			return exist;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
}
