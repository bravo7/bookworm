package bookworm_app.view.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bookworm_app.view.components.screenmenu.BookPanel;
import bookworm_app.view.components.screenmenu.BookWormPanel;
import bookworm_app.view.components.screenmenu.BorrowPanel;
import bookworm_app.view.components.screenmenu.MemberPanel;


public class ScreenMenu extends JPanel{
	//properties
	ArrayList<String> menus = null;
	
	//constructors
	public ScreenMenu()
	{
		this.menus = new ArrayList<String>( Arrays.asList("Members", "Available Book(s)", "Borrowed Book(s)") );
		this.setBackground(new Color(222, 231, 247));
		this.setLayout(new BorderLayout());
		this.setBorder(new EmptyBorder(20, 20, 20, 20));
	}
	
	//methods
	public void changeScreenDisplay(int index)
	{
		switch(index) {
			case 0: this.displayMembers();
				break;
			case 1: this.displayAvaiblableBooks();
				break;
			case 2: this.displayBorrowedBooks();
				break;
		}
	}
	public void changePeople(int peopleId)
	{
		this.clear();
		this.add(new BookWormPanel(peopleId), BorderLayout.CENTER);
	}
	
	public void clearPanel()
	{
		this.clear();
	}
	private void displayMembers()
	{
		this.clear();
		this.add(new MemberPanel(), BorderLayout.CENTER);
	}
	private void displayAvaiblableBooks()
	{
		this.clear();
		this.add(new BookPanel(), BorderLayout.CENTER);
	}
	private void displayBorrowedBooks()
	{
		this.clear();
		this.add(new BorrowPanel(), BorderLayout.CENTER);
	}
	private void clear()
	{
		this.removeAll();
		this.revalidate();
		this.repaint();
	}
	
	//accessors
	public ArrayList<String> getMenus()
	{
		return this.menus;
	}
}
