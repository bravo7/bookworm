package bookworm_app.view.components.screenmenu;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bookworm_app.model.Book;
import bookworm_app.model.People;

public class BookPopUp extends JFrame {

	BookPanel owner;
	int index;
	public BookPopUp(BookPanel panel)
	{
		super();
		this.owner = panel;
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		this.setSize(500, 200);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		this.setContentPane(this.buildContent());
		this.setResizable(false);
		this.setVisible(true);
	}
	
	public JPanel buildContent()
	{
		BookPopUp that = this;
		JPanel content = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.insets = new Insets(8, 10, 5, 8);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		content.add(new JLabel("Title : "),c);
		c.gridy = 0;
		c.gridx = 1;
		JTextField jTitle = new JTextField(13);
		content.add(jTitle,c);
		c.gridy++;
		c.gridx = 1;
		JButton b = new JButton("Ok");
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				Book b;
				that.owner.setNewBook(new Book(jTitle.getText()));
				that.dispose();
				
			}
		});
		content.add(b,c);
		
		return content;
	}
	
}
