package bookworm_app.controller;

import java.util.ArrayList;

import bookworm_app.dbaccess.BookAccess;
import bookworm_app.dbaccess.BorrowAccess;
import bookworm_app.model.Book;

public class BookController {
	public static ArrayList<Book> getBooks()
	{
		return BookAccess.getBooks();
	}
	public static boolean createBook(Book book)
	{
		return BookAccess.createBook(book);
	}
	public static boolean deleteBook(Book book)
	{
		return BookAccess.deleteBook(book.getTitle());
	}
	public static boolean editBook(String oldTitle,Book book)
	{
		return BookAccess.editBook(book.getTitle(), oldTitle);
	}
	public static ArrayList<Book> getAvailableBooks()
	{
		ArrayList<Book> res= new ArrayList<Book>();
		for(Book b : BookController.getBooks())
		{
			String title = b.getTitle();
			if(BorrowAccess.isFree(title))
				res.add(b);
		}
		return res ;
	}
}
