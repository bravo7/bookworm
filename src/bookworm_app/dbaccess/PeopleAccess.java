package bookworm_app.dbaccess;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;

import bookworm_app.Main;
import bookworm_app.model.People;
import bookworm_app.model.dbaccess.utils.DBConnectionBookWorm;

public class PeopleAccess {
	public static boolean createPeople(People people)
	{
		String req = "INSERT INTO people (firstname,lastname,grade) VALUES "
				+ "('"+people.getFirstname()+"','"+people.getLastname()+"'"
				+ ",'"+people.getGrade()+"'); ";
		try {
			int result = (int) DBConnectionBookWorm.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	
	public static ArrayList<People> getPeoples()
	{
		ArrayList<People> resultList = new ArrayList<People>();
		String req = "SELECT * FROM people";
		try {
			ResultSet result = (ResultSet) DBConnectionBookWorm.doRequest(req);
			while (result.next()) {
				People p = new People(result.getInt("id"),result.getString("firstname"), result.getString("lastname"),result.getString("grade"));		
				resultList.add(p);
			}			
			
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return resultList;
	}
	public static boolean deletePeople(int id)
	{
		String req = "DELETE FROM people where id="+id+";";
		try {
			int result = (int)DBConnectionBookWorm.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	public static People getPeopleById(int id)
	{
		String req = "SELECT * FROM people where id = "+id;
		People res = null;
		try {
			ResultSet result = (ResultSet) DBConnectionBookWorm.doRequest(req);
			while (result.next()) {
				People p = new People(result.getInt("id"),result.getString("firstname"), result.getString("lastname"),result.getString("grade"));
				res = p;
			}			
			
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return res;
	}
	public static boolean editPeople(int id, String firstName, String lastName,String grade)
	{
		String req = "UPDATE people set firstname='"+firstName+"',lastname='"+lastName+", grade='"+grade+"' where id = "+id;
		try {
			int result = (int)DBConnectionBookWorm.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
}
