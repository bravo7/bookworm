package bookworm_app.view;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.logging.Level;

import javax.swing.JFrame;

import bookworm_app.Main;
import bookworm_app.model.dbaccess.utils.DBConnectionBookWorm;

public class MainPanel extends JFrame{
	public MainPanel()
	{
		super("BookWorm");
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		this.setSize(1200, 650);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
		  public void windowClosing(WindowEvent we) {
				try {
					DBConnectionBookWorm.closeConnection();
				} catch (SQLException e) {
					Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
				}
		  }
		});
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		this.setContentPane(new MainContent());
		this.setResizable(true);
		this.setVisible(true);
	}
}
