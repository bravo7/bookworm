package bookworm_app.model;

public class Book {
	private String title;
	
	public Book(String title)
	{
		this.title = title;
	}
	
	public void setTitle(String value)
	{
		this.title = value;
	}
	public String getTitle()
	{
		return this.title;
	}
}
