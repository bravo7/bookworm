package bookworm_app;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import bookworm_app.model.dbaccess.utils.DBConnectionBookWorm;
import bookworm_app.view.MainPanel;


public class Main {

	public static Logger LOGGER = null;
	public static void main(String[] args) {
				
		try {
			LOGGER = Logger.getLogger(Main.class.getName());
			FileHandler fileTxt = new FileHandler(System.getProperty("user.dir")+"/log.out");
			SimpleFormatter formatterTxt = new SimpleFormatter();
	        fileTxt.setFormatter(formatterTxt);
	        LOGGER.addHandler(fileTxt);			
	        
			DBConnectionBookWorm.establishConnection();
			new MainPanel();
			
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
	}


}
