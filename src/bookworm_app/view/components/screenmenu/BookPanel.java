package bookworm_app.view.components.screenmenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import bookworm_app.controller.BookController;
import bookworm_app.controller.PeopleController;
import bookworm_app.model.Book;
import bookworm_app.model.People;

public class BookPanel extends JPanel{
	private JTable table = null;
	public BookPanel()
	{		
		BookPanel that = this;
		this.setLayout(new BorderLayout());
		this.setBackground(new Color(222, 231, 247));
		ArrayList<Book> books = BookController.getAvailableBooks();
		DefaultTableModel model = new DefaultTableModel();
		String[] columns = new String[] {"Title"};
		for(String column : columns)
			model.addColumn(column);
		String[] data = new String[1]; 
		for(int i = 0; i < books.size(); i++)
		{
			data[0] = ""+books.get(i).getTitle();
			model.addRow(data);
			
		}
		table = new JTable(model){public boolean isCellEditable(int rowIndex, int colIndex){return false;}};
		table.setRowSelectionAllowed(true);
		this.add(new JScrollPane(table));		
		JPanel jpanelButton = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JButton button = new JButton("Nouveau");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				new BookPopUp(that);
			}
		});
		JButton removeButton = new JButton("Supprimer");
		removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				if(table.getSelectedRow() != -1)
				{
					String bookTitle = table.getValueAt(table.getSelectedRow(), 0).toString();
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					if(	BookController.deleteBook(new Book(bookTitle)));
						model.removeRow(table.getSelectedRow());
				}
			}
		});
		jpanelButton.add(removeButton);
		jpanelButton.add(button);
		this.add(jpanelButton, BorderLayout.SOUTH);
	}
	private void refresh()
	{
		DefaultTableModel model = (DefaultTableModel)table.getModel(); 
		int rows = model.getRowCount(); 
		for(int i = rows - 1; i >=0; i--)
		{
		   model.removeRow(i); 
		}
		ArrayList<Book> books = BookController.getBooks();
		String[] data = new String[5]; 
		for(int i = 0; i < books.size(); i++)
		{
			data[0] = books.get(i).getTitle();
			model.addRow(data);			
		}
	}
	public void setNewBook(Book b)
	{
		Boolean hasBeenCreated = BookController.createBook(b);
		if(hasBeenCreated)
		{
			refresh();
		}					
	}
}
