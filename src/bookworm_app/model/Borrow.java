package bookworm_app.model;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Borrow {
	private int peopleId;
	private String bookTitle;
	private Date borrowDate;
	
	public Borrow(int people, String book, Date date)
	{
		this.peopleId = people;
		this.bookTitle = book;
		this.borrowDate = date;
	}
	public Borrow(int people, String book, String date) throws ParseException
	{
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
	    java.sql.Date sqlDate;
	    sqlDate = new java.sql.Date(sdf1.parse(date).getTime());
		this.peopleId = people;
		this.bookTitle = book;
		this.borrowDate = sqlDate;
	}
	
	public int getPeopleId()
	{
		return this.peopleId;
	}
	public String getBookTitle()
	{
		return this.bookTitle;
	}
	public Date getBorrowDate()
	{
		return this.borrowDate;
	}
	public String getStringDate()
	{
		 DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
         return dateFormat.format(this.borrowDate);  
	}
	
	public void setPeopleId(int value)
	{
		this.peopleId = value;
	}
	public void setBookTitle(String value)
	{
		this.bookTitle = value;
	}
	public void setBorrowDate(Date value)
	{
		this.borrowDate = value;
	}
}
