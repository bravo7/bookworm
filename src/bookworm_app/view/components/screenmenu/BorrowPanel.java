package bookworm_app.view.components.screenmenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import bookworm_app.controller.BorrowController;
import bookworm_app.controller.PeopleController;
import bookworm_app.model.Borrow;
import bookworm_app.model.People;

public class BorrowPanel extends JPanel{
	private JTable table = null;
	public BorrowPanel()
	{		
		BorrowPanel that = this;
		this.setLayout(new BorderLayout());
		this.setBackground(new Color(222, 231, 247));
		ArrayList<Borrow> borrows = BorrowController.getBorrows();
		DefaultTableModel model = new DefaultTableModel();
		String[] columns = new String[] {"Title","Member", "Date"};
		for(String column : columns)
			model.addColumn(column);
		String[] data = new String[3]; 
		for(int i = 0; i < borrows.size(); i++)
		{
			data[0] = borrows.get(i).getBookTitle();
			People p = PeopleController.getPeopleById(borrows.get(i).getPeopleId());
			data[1] = p.getFullName();
			data[2] = borrows.get(i).getStringDate();
			model.addRow(data);
			
		}
		table = new JTable(model){public boolean isCellEditable(int rowIndex, int colIndex){return false;}};
		this.add(new JScrollPane(table));		
	}
}
