package bookworm_app.view.components.screenmenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import bookworm_app.controller.PeopleController;
import bookworm_app.model.People;

public class MemberPanel extends JPanel{
	private JTable table = null;
	public MemberPanel()
	{		
		MemberPanel that = this;
		this.setLayout(new BorderLayout());
		this.setBackground(new Color(222, 231, 247));
		ArrayList<People> peoples = PeopleController.getPeoples();
		DefaultTableModel model = new DefaultTableModel();
		String[] columns = new String[] {"Id","Firstname", "Lastname", "Grade"};
		for(String column : columns)
			model.addColumn(column);
		String[] data = new String[4]; 
		for(int i = 0; i < peoples.size(); i++)
		{
			data[0] = ""+peoples.get(i).getId();
			data[1] = peoples.get(i).getFirstname();
			data[2] = peoples.get(i).getLastname();
			data[3] = peoples.get(i).getGrade();
			model.addRow(data);
			
		}
		table = new JTable(model){public boolean isCellEditable(int rowIndex, int colIndex){return false;}};
		table.setRowSelectionAllowed(true);
		table.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent event) {
				if (event.getClickCount() == 2) {
			      JTable target = (JTable)event.getSource();
			      int row = target.getSelectedRow();
			      int id = Integer.parseInt(table.getValueAt(table.getSelectedRow(), 0).toString());
			      String firstName = table.getValueAt(table.getSelectedRow(), 1).toString();
			      String lastName = table.getValueAt(table.getSelectedRow(), 2).toString();
			      String grade = table.getValueAt(table.getSelectedRow(), 3).toString();
				  People people = new People(id,firstName, lastName,grade);
				  people.setId(id);
				  new MemberPopUp(that, people, row);				      
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {}
			@Override
			public void mouseExited(MouseEvent arg0) {}
			@Override
			public void mousePressed(MouseEvent arg0) {}
			@Override
			public void mouseReleased(MouseEvent arg0) {}
		});
		this.add(new JScrollPane(table));		
		JPanel jpanelButton = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JButton button = new JButton("New");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				new MemberPopUp(that, null, -1);
			}
		});
		JButton removeButton = new JButton("Delete");
		removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				if(table.getSelectedRow() != -1)
				{
					String peopleID = table.getValueAt(table.getSelectedRow(), 0).toString();
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					if(	PeopleController.deletePeople(Integer.parseInt(peopleID)))
						model.removeRow(table.getSelectedRow());
				}
			}
		});
		jpanelButton.add(removeButton);
		jpanelButton.add(button);
		this.add(jpanelButton, BorderLayout.SOUTH);
	}
	private void refresh()
	{
		DefaultTableModel model = (DefaultTableModel)table.getModel(); 
		int rows = model.getRowCount(); 
		for(int i = rows - 1; i >=0; i--)
		{
		   model.removeRow(i); 
		}
		ArrayList<People> peoples = PeopleController.getPeoples();
		String[] data = new String[5]; 
		for(int i = 0; i < peoples.size(); i++)
		{
			data[0] = ""+peoples.get(i).getId();
			data[1] = peoples.get(i).getFirstname();
			data[2] = peoples.get(i).getLastname();
			data[3] = peoples.get(i).getGrade();
			model.addRow(data);			
		}
	}
	public void setPeople(People p,int index)
	{
		Boolean hasBeenCreated = PeopleController.editPeople(p);
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		String[] data = new String[3]; 		
		data[0] = ""+p.getId();
		data[1] = p.getFirstname();
		data[2] = p.getLastname();
		data[3] = p.getGrade();
	
		for(int i = 0; i<3;i++)
		{
			model.setValueAt(data[i], index, i);
		}
		
		if(hasBeenCreated)
			refresh();
	}
	public void setNewPeople(People p)
	{
		Boolean hasBeenCreated = PeopleController.createPeople(p);
		if(hasBeenCreated)
			refresh();
		
	}
}
