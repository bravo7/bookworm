package bookworm_app.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

import bookworm_app.view.components.MainMenu;
import bookworm_app.view.components.ScreenMenu;


public class MainContent extends JPanel{
	public MainContent()
	{
		this.setLayout(new BorderLayout());
		ScreenMenu screen = new ScreenMenu();
		this.add(new MainMenu(screen),BorderLayout.WEST);
		this.add(screen, BorderLayout.CENTER);
		this.setBackground(new Color(0,0,0));
	}
}
