package bookworm_app.dbaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;

import bookworm_app.Main;
import bookworm_app.model.Book;
import bookworm_app.model.People;
import bookworm_app.model.dbaccess.utils.DBConnectionBookWorm;

public class BookAccess {
	public static boolean createBook(Book book)
	{
		String req = "INSERT INTO book (title) VALUES "
				+ "('"+book.getTitle()+"');";
		try {
			int result = (int) DBConnectionBookWorm.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	
	public static ArrayList<Book> getBooks()
	{
		ArrayList<Book> resultList = new ArrayList<Book>();
		String req = "SELECT * FROM book";
		try {
			ResultSet result = (ResultSet) DBConnectionBookWorm.doRequest(req);
			while (result.next()) {
				Book p = new Book(result.getString("title"));		
				resultList.add(p);
			}			
			
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return resultList;
	}
	public static boolean deleteBook(String title)
	{
		String req = "DELETE FROM book where title='"+title+"';";
		try {
			int result = (int)DBConnectionBookWorm.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	public static Book getBook(String title)
	{
		String req = "SELECT * FROM book where title = '"+title+"';";
		Book res = null;
		try {
			ResultSet result = (ResultSet) DBConnectionBookWorm.doRequest(req);
			while (result.next()) {
				Book p = new Book(result.getString("title"));
				res = p;
			}			
			
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return res;
	}
	public static boolean editBook(String title, String oldTitle)
	{
		String req = "UPDATE book set title='"+title+"' where title = '"+oldTitle+"';";
		try {
			int result = (int)DBConnectionBookWorm.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
}
