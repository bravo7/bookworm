package bookworm_app.controller;

import java.util.ArrayList;

import bookworm_app.dbaccess.BorrowAccess;
import bookworm_app.model.Borrow;

public class BorrowController {
	public static ArrayList<Borrow> getBorrows()
	{
		return BorrowAccess.getBorrows();
	}
	public static boolean createBorrow(Borrow borrow)
	{
		return BorrowAccess.createBorrow(borrow);
	}
	public static boolean deleteBorrow(String bookTitle)
	{
		return BorrowAccess.deleteBorrow(bookTitle);
	}
}
