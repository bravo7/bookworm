package bookworm_app.view.components.screenmenu;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import bookworm_app.Main;
import bookworm_app.controller.BookController;
import bookworm_app.controller.BorrowController;
import bookworm_app.controller.PeopleController;
import bookworm_app.model.Book;
import bookworm_app.model.Borrow;
import bookworm_app.model.People;
public class BookWormPanel extends JPanel{
	
	private JComboBox booksCombo;
	private boolean firstLoad;
	private JButton freeButton;
	private JTable table;
	private int people;
	public BookWormPanel(int peopleId)
	{
		people = peopleId;
		People people = PeopleController.getPeopleById(peopleId);
		if(people != null)
		{
			this.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			c.weightx = 1;
			c.insets = new Insets(8, 10, 5, 8);
			c.anchor = GridBagConstraints.NORTHWEST;
			this.add(new JLabel("Member : "),c);
			c.gridy ++;
			this.add(new JLabel("Book to borrow : "),c);
			c.gridy = 0;
			c.gridx++;
			JTextField jPeople = new JTextField(15);
			jPeople.setEditable(false);
			jPeople.setText(people.getFullName());
			this.add(jPeople,c);
			c.gridy ++;
			getAvailableBooks();
			this.add(booksCombo,c);
			booksCombo.addActionListener(new ActionListener() {
		        @Override
		        public void actionPerformed(ActionEvent e) {
		        	if(!firstLoad)
					{
						String bookname = (String)booksCombo.getSelectedItem();
						DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
						Date date = new Date();
						String dateString = dateFormat.format(date);
						try {
							Borrow newBorrow = new Borrow(peopleId,bookname,dateString);
							BorrowController.createBorrow(newBorrow);
							refreshBorrowedBooks();
							getAvailableBooks();
						} catch (ParseException ex) {
							Main.LOGGER.log(Level.SEVERE,ex.getMessage(),ex);
						}
						
					}
		        }
		    });
			c.gridy = 3;
			c.gridx = 0;
			refreshBorrowedBooks();
			this.add(new JScrollPane(table),c);	
			c.gridx++;
			c.weighty = 0.6;
			freeButton = new JButton("Free Book");
			freeButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e)
				{
					if(table.getSelectedRow() != -1)
					{
						String bookTitle = table.getValueAt(table.getSelectedRow(), 0).toString();
						DefaultTableModel model = (DefaultTableModel) table.getModel();
						if(	BorrowController.deleteBorrow(bookTitle));
							model.removeRow(table.getSelectedRow());
						getAvailableBooks();
					}
				}
			});
			this.add(freeButton,c);
		}
		firstLoad = false;
	}
	public void getAvailableBooks()
	{		
		ArrayList<Book> books = BookController.getAvailableBooks();
		ArrayList<String> booksString = new ArrayList<>();
		DefaultComboBoxModel model = null;
		booksString.add(0,"");
		for(Book b : books)
		{
			booksString.add(b.getTitle());
		}
		if(booksCombo == null)
		{
			booksCombo = new JComboBox();
			booksCombo.setPreferredSize(new Dimension(300,(int)booksCombo.getPreferredSize().getHeight()));
		}
		model = new DefaultComboBoxModel(booksString.toArray());
		booksCombo.setModel(model);
	}
	public void refreshBorrowedBooks()
	{
		ArrayList<Borrow> borrows = BorrowController.getBorrows();
		DefaultTableModel model = null;
		if(table == null)
		{
			model = new DefaultTableModel();
			String[] columns = new String[] {"Title", "Date"};
			for(String column : columns)
				model.addColumn(column);
		}
		else
			model = (DefaultTableModel)table.getModel(); 		
		
		while(model.getRowCount() > 0)
		{
			model.removeRow(0);
		}
		String[] data = new String[2]; 
		for(int i = 0; i < borrows.size(); i++)
		{ 
			Borrow borrow = borrows.get(i);
			if(borrow.getPeopleId() == people)
			{
				data[0] = borrow.getBookTitle();			
				data[1] = borrow.getStringDate();
				model.addRow(data);
			}			
		}
		if(table == null)
			table = new JTable(model){public boolean isCellEditable(int rowIndex, int colIndex){return false;}};
		
	}
}
