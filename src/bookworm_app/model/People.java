package bookworm_app.model;

public class People {
	private int id;
	private String firstname;
	private String lastname;
	private String grade;
	
	public People(int id ,String firstname, String lastname, String grade)
	{
		this.id = id;
		this.lastname = lastname;
		this.firstname = firstname;
		this.grade = grade;
	}
	
	public int getId()
	{
		return this.id;
	}
	public String getLastname ()
	{
		return this.lastname;
	}	
	public String getFirstname()
	{
		return this.firstname;
	}
	public String getGrade()
	{
		return this.grade;
	}
	
	public void setId(int value)
	{
		this.id = value;
	}
	public void setFirstname(String value)
	{
		this.firstname = value;
	}
	public void setLastname(String value)
	{
		this.lastname = value;
	}
	public void setGrade(String value)
	{
		this.grade = value;
	}

	public String getFullName() {
		return this.firstname + " " +this.lastname;
	}
}
